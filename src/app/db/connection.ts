import { createPool } from "mysql2/promise";

const pool = createPool({
	port: 3306,
	host: "localhost",
	user: "root",
	password: "queso",
	database: "united",
});

export { pool };
