import express from "express";
import { routerPayment } from "./payments";
import { routerUsers } from "./users";

const routerMain = express.Router();

routerMain.use("/payments", routerPayment);
routerMain.use("/users", routerUsers);

export { routerMain };
