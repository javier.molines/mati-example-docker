import { Request, Response } from "express";
import { pool } from "../../db/connection";

const addPayment = async (req: Request, res: Response) => {
	const { amount, code } = req.body;

	const prefix = "INSERT INTO payments (monto, codigo) VALUES";
	const contentQuery = `${prefix} ('${amount}', '${code}')`;
	await pool.query(contentQuery);

	res.json({
		msg: "Add payment",
	});
};

export { addPayment };
