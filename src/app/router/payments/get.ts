import { Request, Response } from "express";
import { pool } from "../../db/connection";

const getPayments = async (req: Request, res: Response) => {
	const querySelect = "SELECT * FROM payments";
	const totalPayments = await pool.query(querySelect);

	res.json({
		msg: "All payments",
		data: totalPayments[0],
	});
};

export { getPayments };
