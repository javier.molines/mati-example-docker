import express from "express";
import { getPayments } from "./get";
import { addPayment } from "./insert";

const routerPayment = express.Router();

routerPayment.route("/").get(getPayments);
routerPayment.route("/").post(addPayment);

export { routerPayment };
