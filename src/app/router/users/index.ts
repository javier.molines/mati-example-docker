import express from "express";
import { getUsers } from "./get";
import { addUser } from "./insert";

const routerUsers = express.Router();

routerUsers.route("/").get(getUsers);
routerUsers.route("/").post(addUser);

export { routerUsers };
