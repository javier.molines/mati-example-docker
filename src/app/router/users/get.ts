import { Request, Response } from "express";
import { pool } from "../../db/connection";

const getUsers = async (req: Request, res: Response) => {
	const querySelect = "SELECT * FROM users";
	const totalUsers = await pool.query(querySelect);

	res.json({
		msg: "All users",
		data: totalUsers[0],
	});
};

export { getUsers };
