import { Request, Response } from "express";
import { pool } from "../../db/connection";

const addUser = async (req: Request, res: Response) => {
	const { nombre, apellido } = req.body;

	const prefix = "INSERT INTO users (nombre, apellido) VALUES";
	const contentQuery = `${prefix} ('${nombre}', '${apellido}')`;
	await pool.query(contentQuery);

	res.json({
		msg: "Add user",
	});
};

export { addUser };
