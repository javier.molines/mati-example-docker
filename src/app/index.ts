import express from "express";
import { pool } from "./db/connection";
import { routerMain } from "./router";

const app = express();
const port = 3000;

app.use(express.json());
app.use("/api", routerMain);

const initial = async () => {
	console.log("Validando base da datos");
	const validAndCreate = "CREATE TABLE IF NOT EXISTS";

	await pool.query(
		`${validAndCreate} payments ( monto varchar(255), codigo varchar(255) );`,
	);

	await pool.query(
		`${validAndCreate} users (nombre varchar(255), apellido varchar(255) );`,
	);
};

app.listen(port, () => {
	initial();
	console.log(`Example app listening on port ${port}`);
});
